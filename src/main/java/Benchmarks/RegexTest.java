package Benchmarks;

import org.apache.commons.lang3.StringUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {

    private static Pattern combined = Pattern.compile("((email(Id)?(_id)?\\\":\\\")(.*?)(@))|" +
                                                              "((mobile(_no)?(Number)?\\\":\\\")(.*?)(\\\"))|" +
                                                              "(([vV]pa\\\":\\\")(.*?)(@))|" +
                                                              "((\\\"cards\\\":\\[)(.*?)(\\]))");
    private static final Pattern mail = Pattern.compile("(email(Id)?(_id)?\\\":\\\")(.*?)(@)");
    private static final Pattern mobile = Pattern.compile("(mobile(_no)?(Number)?\\\":\\\")(.*?)(\\\")");
    private static final Pattern vpa = Pattern.compile("([vV]pa\\\":\\\")(.*?)(@)");
    private static final Pattern card = Pattern.compile("(\\\"cards\\\":\\[)(.*?)(\\])");

    public static String mask(String string,
                              int maskEndIndex) {

        switch (maskEndIndex) {

            case -1:
            case 0:
                return string;
            case 1:
                return "*" + string.substring(maskEndIndex);
            case 2:
                return string.substring(0, 1) + "*" + string.substring(maskEndIndex);
            case 3:
                return string.substring(0, 1) + "*" + string.substring(maskEndIndex - 1);
            case 4:
                return string.substring(0, 1) + "**" + string.substring(maskEndIndex - 1);
            case 5:
                return string.substring(0, 1) + "**" + string.substring(maskEndIndex - 2);
            default:
                return string.substring(0, 2) + "**" + string.substring(maskEndIndex - 2);
        }
    }

    public static String tfunc(Matcher m) {

        if (StringUtils.isNotBlank(m.group(1))) {
            return m.group(2) + mask(m.group(5), m.group(5).length()) + m.group(6);
        }
        else if (StringUtils.isNotBlank(m.group(7))) {
            return m.group(8) + mask(m.group(11), m.group(11).length()) + m.group(12);
        }
        else if (StringUtils.isNotBlank(m.group(13))) {
            return m.group(14) + mask(m.group(15), m.group(15).length()) + m.group(16);
        }
        else if (StringUtils.isNotBlank(m.group(17))) {
            return m.group(18) + m.group(20);
        }
        return null;
    }

    public static String regexMaskAllCombined(String message) {

        try {
            StringBuffer sb = new StringBuffer();
            Matcher m = combined.matcher(message);
//            int j = -1;
            while (m.find()) {
//                j++;
//                for(int i =0;i<=m.groupCount();i++){
//                    System.out.println(j+" "+i+" "+m.group(i));
//                }
                m.appendReplacement(sb, tfunc(m));
            }
            m.appendTail(sb);
            return sb.toString();
        }
        catch (Exception e) {
            //group mismatch such as group 2 or 3 doesnt exist, unlikely as check on m.find()
            System.out.println(("Error while masking for all in message: " + message));
            return message;
        }
    }

    public static String regexMaskEmail(String message) {

        try {
            StringBuffer sb = new StringBuffer();
            //group 1 = email(Id)?(_id)?\\\":\\\", group 2 = Id, group3 = _id, group4 = .*, group5 = @
            Matcher m = mail.matcher(message);
            while (m.find()) {
                m.appendReplacement(sb, m.group(1) + mask(m.group(4), m.group(4).length()) + m.group(5));
            }
            m.appendTail(sb);
            return sb.toString();
        }
        catch (Exception e) {
            //group mismatch such as group 4 or 5 doesnt exist, unlikely as check on m.find()
            System.out.println("Error while masking for email in message: {}" + message);
            return message;
        }
    }

    public static String regexMaskMobile(String message) {

        try {
            StringBuffer sb = new StringBuffer();
            //group 1 = mobile(_no)?(Number)?\\\":\\\"), group 2 = _no, group3 = Number, group4=.*, group5 = @
            Matcher m = mobile.matcher(message);
            while (m.find()) {
                m.appendReplacement(sb, m.group(1) + mask(m.group(4), m.group(4).length()) + m.group(5));
            }
            m.appendTail(sb);
            return sb.toString();
        }
        catch (Exception e) {
            //group mismatch such as group 2 or 3 doesnt exist, unlikely as check on m.find()
            System.out.println("Error while masking for vpa in message: {}" + message);
            return message;
        }
    }

    public static String regexMaskVPA(String message) {

        try {
            StringBuffer sb = new StringBuffer();
            Matcher m = vpa.matcher(message);
            while (m.find()) {
                m.appendReplacement(sb, m.group(1) + mask(m.group(2), m.group(2).length()) + m.group(3));
            }
            m.appendTail(sb);
            return sb.toString();
        }
        catch (Exception e) {
            //group mismatch such as group 2 or 3 doesnt exist, unlikely as check on m.find()
            System.out.println("Error while masking for vpa in message: {}" + message);
            return message;
        }
    }

    public static String regexMaskCard(String message) {

        try {
            StringBuffer sb = new StringBuffer();
            Matcher m = card.matcher(message);
            while (m.find()) {
                m.appendReplacement(sb, m.group(1) + m.group(3));
            }
            m.appendTail(sb);
            return sb.toString();
        }
        catch (Exception e) {
            //group mismatch such as group 2 or 3 doesnt exist, unlikely as check on m.find()
            System.out.println("Error while masking for vpa in message: {}" + message);
            return message;
        }
    }

    public static String regexMaskAll(String message) {

        return regexMaskEmail(regexMaskMobile(regexMaskVPA(regexMaskCard(message))));
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 2, time = 5, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 5, time = 5, timeUnit = TimeUnit.SECONDS)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public static String control(BenchMarkState state) {

        return state.preMask;
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 2, time = 5, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 5, time = 5, timeUnit = TimeUnit.SECONDS)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public static String regexMaskAllCombined(BenchMarkState state) {

        return regexMaskAllCombined(state.preMask);
    }


    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 2, time = 5, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 5, time = 5, timeUnit = TimeUnit.SECONDS)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public static String regexMaskAll(BenchMarkState state) {

        return regexMaskAll(state.preMask);
    }

    @State(Scope.Benchmark)
    public static class BenchMarkState {

        public String preMask = "\"this is my email\":\"testing@gmail.com\","
                + "\"this is my mobile\":\"98876543210\","
                + "\"this is my vpa\":\"tes@axis\","
                + "\"secondary emailId\":\"a@yahoo.com\","
                + "\"tertiary email_id\":\"cd@hotmail.com\","
                + "\"secondary customerVpa\":\"test@okicici\","
                + "former \"cards\":[{\"XXXXXXX\"}],latter,"
                + "\"secondary mobile_no\":\"1234567890\"";
    }


    public static void main(String[] args) throws Exception {

        org.openjdk.jmh.Main.main(args);
    }
}
